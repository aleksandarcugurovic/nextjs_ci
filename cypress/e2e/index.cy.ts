/// <reference types="cypress"/>

describe("Title Check", () => {
  it("Should check if title is correct", () => {
    cy.visit("http://localhost:3000/");
    cy.get("h1").contains("Welcome to Next.js");
  });
});

export {};
